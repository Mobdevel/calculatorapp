package com.example.calculatorapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
    var n =true
    var o = ""
    var a = ""
    fun numberClick(view: View){
        if(n)
            textView.setText("")
        n =false

        var backclick  = textView.text.toString()
        var backclicks = view as Button
        when(backclicks.id){
            number0.id -> {backclick += "0"}
            number1.id -> {backclick += "1"}
            number2.id -> {backclick += "2"}
            number3.id -> {backclick += "3"}
            number4.id -> {backclick += "4"}
            number5.id -> {backclick += "5"}
            number6.id -> {backclick += "6"}
            number7.id -> {backclick += "7"}
            number8.id -> {backclick += "8"}
            number9.id -> {backclick += "9"}
            number_dot.id -> {backclick += "."}
            number_plus_minus_btn.id -> {backclick = "-$backclick"}

        }
        textView.setText(backclick)


    }
    fun number_operator(view: View){
        n = true
        o =textView.text.toString()
        var backclicks = view as Button
        when(backclicks.id){
            number_multiply.id -> {a = "*"}
            number_plus.id -> {a = "+"}
            number_minus.id -> {a = "-"}
            number_slash.id -> {a = "/"}

        }

    }
    fun equal(view: View){
        var nnumber = TextView.text.toString()
        var result = 0
        when(a){
            "+" -> {result = o.toInt() + nnumber.toInt()}
            "*" -> {result = o.toInt() * nnumber.toInt()}
            "/" -> {result = o.toInt() / nnumber.toInt()}
            "-" -> {result = o.toInt() - nnumber.toInt()}


        }
        textView.setText((result.toString()))
    }
    fun num_c(view: View){
        textView.setText("0")
        n = true

    }


}
